using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitpoints : MonoBehaviour
{
    [SerializeField] bool isPlayer;
    [SerializeField] int hitPoints = 50;
    [SerializeField] int scorePoints = 50;
    [SerializeField] ParticleSystem hitEffect;
    [SerializeField] bool applyCameraShake;
    CameraShake cameraShake;
    [Header("Audio")]
    AudioPlayer audioPlayer;
    ScoreKeeper score;

    LevelManager levelManager;

    void Awake()
    {
        cameraShake = Camera.main.GetComponent<CameraShake>();
        audioPlayer = FindObjectOfType<AudioPlayer>();
        score = FindObjectOfType<ScoreKeeper>();
        levelManager = FindObjectOfType<LevelManager>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.GetComponent<DamageDealer>();
        if (damageDealer != null)
        {
            TakeDamage(damageDealer.GetDamage());
            audioPlayer.PlayDamageClip();
            PlayHitEffect();
            ShakeCamera();
            damageDealer.Hit();
        }
    }

    void TakeDamage(int damageAmount)
    {
        hitPoints -= damageAmount;
        if (hitPoints <= 0)
        {
            Die();

        }
    }

    void Die()
    {
        if (!isPlayer)
        {
            score.increaseScore(scorePoints);
        }
        else
        {
            levelManager.LoadGameOver();
        }
        Destroy(gameObject);
    }

    void PlayHitEffect()
    {
        if (hitEffect != null)
        {
            //This creates an instance of our particle effect - Quaternion as it requires a roation attribute
            ParticleSystem instance = Instantiate(hitEffect, transform.position, Quaternion.identity);

            //This destory the instance after the set duration of the animation. We don't want it to stop at the min loop.
            Destroy(instance.gameObject, instance.main.duration + instance.main.startLifetime.constantMax);
        }
    }

    void ShakeCamera()
    {
        if (cameraShake != null && applyCameraShake)
        {
            cameraShake.Play();
        }
    }

    public int getHitpoints()
    {
        return hitPoints;
    }


}
