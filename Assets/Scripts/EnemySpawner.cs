using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    WaveConfig currentWave;
    [SerializeField] List<WaveConfig> waveConfigs;
    [SerializeField] float timeBetweenWaves = 0.2f;
    [SerializeField] bool isLooping = true;


    void Start()
    {

        StartCoroutine(SpawnEnemyWaves());
    }

    //Uses a Coroutine. We added IEnumerator as the reutn type.
    //Than yield return is setting the time before it can loop back.
    private IEnumerator SpawnEnemyWaves()
    {
        do
        {
            foreach (WaveConfig wave in waveConfigs)
            {
                currentWave = wave;


                for (int i = 0; i < currentWave.GetEnemyCount(); i++)
                {


                    Instantiate(currentWave.GetEnemyPrefab(i), currentWave.GetStartingWayPoint().position, Quaternion.identity, transform);
                    yield return new WaitForSeconds(currentWave.GetRandomSpawnTime());
                }

                yield return new WaitForSeconds(timeBetweenWaves);
            }
            //isLooping = false; if we want a finite wave amount
        } while (isLooping);
    }

    public WaveConfig GetCurrentWave()
    {
        return currentWave;
    }
}
