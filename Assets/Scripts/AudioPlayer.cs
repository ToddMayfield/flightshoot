using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    [Header("Shooting")]
    [SerializeField] AudioClip shootingClip;
    [SerializeField] [Range(0f, 1f)] float shootingVolume = 1f;
    [Header("Damage")]
    [SerializeField] AudioClip damagingClip;
    [SerializeField] [Range(0f, 1f)] float damagingVolume = 1f;

    public void PlayShootingClip()
    {
        // if (shootingClip != null)
        // {
        //     AudioSource.PlayClipAtPoint(shootingClip, Camera.main.transform.position, shootingVolume);
        // }

        PlayClip(shootingClip, shootingVolume);
    }

    public void PlayDamageClip()
    {
        // if (damagingClip != null)
        // {
        //     AudioSource.PlayClipAtPoint(damagingClip, Camera.main.transform.position, damagingVolume);
        // }
        PlayClip(damagingClip, shootingVolume);
    }

    void PlayClip(AudioClip clip, float volume)
    {
        if (clip != null)
        {
            Vector3 cameraPos = Camera.main.transform.position;
            AudioSource.PlayClipAtPoint(clip, cameraPos, volume);
        }
    }

}
